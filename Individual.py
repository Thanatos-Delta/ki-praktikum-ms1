class Individual():
    def __init__(self, fitness, path: list[int]):
        self.fitness = fitness
        self.path = path

    def __eq__(self, other):
        return self.path == other.path