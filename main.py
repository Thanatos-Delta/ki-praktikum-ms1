import os
import random
import tsplib95
import matplotlib.pyplot as plt

from Individual import Individual



# Globale Variablen
tsp_instance = None

# Lade TSP-Instanz
def read_file(file):
    global tsp_instance
    tsp_instance = tsplib95.load(file)

# Erhalte die Liste der Städte als Python-Liste
def get_cities_as_list():
    return list(tsp_instance.get_nodes())

# Berechne die Fitness eines Pfads
def get_fitness(path):
    fitness = 0
    for i in range(len(path) - 1):
        fitness += get_distance(path[i], path[i + 1])
        fitness += get_distance(path[-1], path[0])  # Add distance between last and first city
    return fitness

# Berechne die Distanz zwischen zwei Städten
def get_distance(city_one, city_two):
    return tsp_instance.get_weight(city_one, city_two)

# Erzeuge einen zufälligen Pfad
def get_random_path(cities):
    random.shuffle(cities)
    return cities

# Benutzerschnittstelle zur Eingabe der Evolutionsparameter
def interface():
    global POPULATION
    global EVO_GENERATIONS
    global EVO_MU
    global EVO_LAMBDA
    global EVO_MUTATION_RATE


# EVO_GENERATIONS = int(input("How many generations do you need?"))
# EVO_MU = int(input("How many parents should be used for recombination/mutation?"))
# EVO_LAMBDA = int(input("How many children should be used for recombination/mutation?"))
# EVO_MUTATION_RATE = float(input("How high should the mutation rate be?"))
# POPULATION = int(input("How many paths should the initial population include?"))

## Low children weiter testen für bessere Exploration!!!

EVO_GENERATIONS = 100000
EVO_MU = 300
EVO_LAMBDA = 100
EVO_MUTATION_RATE = 0.25
POPULATION = 1000

# # For debugging purposes
# EVO_GENERATIONS = 500
# EVO_MU = 600
# EVO_LAMBDA = 400
# EVO_MUTATION_RATE = 1
# POPULATION = 1000

# Plus-Selection: Elternauswahl, Mutation und Rekombination
def plus_selection(evo_mu, evo_lambda, population_list, evo_mutation_rate, all_cities):
    fittest_parents = sorted(population_list, key=lambda individuum: individuum.fitness)
    fittest_parents = fittest_parents[:evo_mu]

    offspring = []

    while len(offspring) < evo_lambda:
        rnd = random.random()

        if rnd < 0.5:
            mutant = mutation(get_random_individual(population_list), evo_mutation_rate)
            offspring.append(mutant)
        else:
            crossover = random.randint(0, len(population_list))
            recombined = recombination(get_random_individual(population_list), get_random_individual(population_list), crossover, all_cities)
            offspring.extend(recombined)

    mu_plus_lambda = fittest_parents + offspring

    mu_plus_lambda_minus_duplicates = []
    for i in mu_plus_lambda:
        if i not in mu_plus_lambda_minus_duplicates:
            mu_plus_lambda_minus_duplicates.append(i)

    fittest_mu_plus_lambda = mu_plus_lambda_minus_duplicates

    return fittest_mu_plus_lambda

# Wähle ein zufälliges Individuum aus der Population
def get_random_individual(individuals):
    return individuals[random.randint(0, len(individuals) - 1)]

# Entscheide, ob eine Mutation stattfinden soll, basierend auf der Mutationsrate
def should_mutate(mutation_rate):
    options = [True, False]
    probabilities = [mutation_rate, 1 - mutation_rate]
    return random.choices(options, probabilities)[0]

# Führe eine Mutation auf einem Individuum durch
def mutation(individuum, mutation_rate):
    path = individuum.path.copy()
    tmp = 0

    for i in range(len(path)):
        if should_mutate(mutation_rate):
            tmp = path[i]
            new_city = random.choice(path)
            path[i] = new_city
            path[path.index(new_city)] = tmp
    fitness = get_fitness(path)  # Recalculate fitness after mutation
    return Individual(fitness, path)

# Repariere eine rekombinierte Lösung, indem fehlende Städte hinzugefügt werden und Duplikate entfernt
def repair_recombination(all_cities, recombinated_path):
    repaired_recombination = []

    for i in recombinated_path:
        if i not in repaired_recombination:
            repaired_recombination.append(i)

    missing_cities = [city for city in all_cities if city not in repaired_recombination]
    for missing_city in missing_cities:
        closest_city, closest_index = min([(repaired_recombination[i], i) for i in range(len(repaired_recombination))],
                                          key=lambda x: get_distance(x[0], missing_city))
        repaired_recombination.insert(closest_index, missing_city)

    return repaired_recombination

# Rekombination zweier Individuen
def recombination(individuum_one, individuum_two, crossover, all_cities):
    individuum_one_first_half = individuum_one.path[:crossover]
    individuum_one_second_half = individuum_one.path[crossover:]
    individuum_two_first_half = individuum_two.path[:crossover]
    individuum_two_second_half = individuum_two.path[crossover:]

    recombinated_path_one = individuum_one_first_half + individuum_two_second_half
    recombinated_path_two = individuum_two_first_half + individuum_one_second_half

    recombinated_path_one = repair_recombination(all_cities, recombinated_path_one)
    recombinated_path_two = repair_recombination(all_cities, recombinated_path_two)

    recombinated_child_one = Individual(get_fitness(recombinated_path_one), recombinated_path_one)  # Recalculate fitness
    recombinated_child_two = Individual(get_fitness(recombinated_path_two), recombinated_path_two)  # Recalculate fitness

    recombinated_children = [recombinated_child_one, recombinated_child_two]

    return recombinated_children

def main():
    # Lade TSP-Instanz
    read_file("a280.tsp")
    #read_file("att48.tsp")

    # Hole die Liste der Städte
    all_cities = get_cities_as_list()

    interface()

    # Initiale Population erzeugen
    population_list = []
    for i in range(POPULATION):
        path = get_random_path(all_cities.copy())
        fitness = get_fitness(path)
        population_list.append(Individual(fitness, path))


    # Store fitnesses for visualization
    fitnesses = []

    # Evolutionsschleife
    for i in range(EVO_GENERATIONS):
        evoluted_generation = plus_selection(EVO_MU, EVO_LAMBDA, population_list, EVO_MUTATION_RATE, all_cities)
        fitnesses.append(evoluted_generation[0].fitness)
        print("Beste Fitness in Generation", i + 1, ":", evoluted_generation[0].fitness)
        population_list = evoluted_generation


    # Erstellen des Plots
    plt.figure(figsize=(24, 22))
    plt.plot(range(EVO_GENERATIONS), fitnesses)
    plt.xlabel('Generation')
    plt.ylabel('Fitness')
    plt.title('Fitness über Generationen')

    text = "EVO_GENERATIONS = " + str(EVO_GENERATIONS) + "\nEVO_MU = " + str(EVO_MU) + "\nEVO_LAMBDA = " + str(EVO_LAMBDA) + "\nEVO_MUTATION_RATE = " + str(EVO_MUTATION_RATE) + "\nPOPULATION = " + str(POPULATION) + "\nBeste Fitness: " + str(evoluted_generation[0].fitness)
    x, y = 0.05, 0.95  # Relative Position anpassen
    plt.figtext(x, y, text, color='red', fontsize=14, ha='left', va='top')

    # Pfad zum Speichern des Plots
    plot_path = os.path.join('plots', 'plot_' + str(evoluted_generation[0].fitness) + ".png")

    plt.savefig(plot_path)
    plt.close()


if __name__ == "__main__":
    main()

